#include iostream

using namespace std;

class CWayPoint
{
	private:
	doulbe m_lat;
	double m_long;
	string m_name;
	
	public:
	void setName( string name );
	void setCoordinates(double lat, double long);
	string getName();
}